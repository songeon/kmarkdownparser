#ifndef TEXTSTYLEFACTORY_H
#define TEXTSTYLEFACTORY_H

#include <QObject>

class QTextCharFormat;

enum TextStyle : unsigned int{
    Normal	    =	0x01,
    H1 		      = 0x02,
    H2 		      = 0x04,
    H3 		      = 0x08,
    H4 		      = 0x10,
    H5 		      = 0x20,
    H6 		      = 0x40,
    Italic	    = 0x80,
    Bold	      =	0x100,
    Underline   = 0x200,
    Cancelline  = 0x400,
    Token       = 0x800

};

class TextStyleFactory : public QObject {
private:
    int normalFontSize = 10;
    int HeaderFontSize[7] = {0, 30, 22, 17, 15, 12, 10 };
public:
    TextStyleFactory();
    QTextCharFormat getByFlag(unsigned int);
};

#endif // TEXTSTYLEFACTORY_H

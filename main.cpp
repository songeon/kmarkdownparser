/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "throw_exception.hpp"

#include "parser.h"
#include "ast/string.hpp"
#include "kmarkdownpart.h"

#include "boost/spirit/home/x3.hpp"
#include "boost/variant/variant.hpp"

#include <QString>

#include <string>
#include <iostream>

using namespace boost::spirit;
using boost::optional;

namespace AST {
  using boost::variant;
  using StrIter = std::string::iterator;
}

int main() {
  std::string str;
  std::getline(std::cin, str);
  
  using boost::spirit::x3::ascii::space;
  using boost::spirit::x3::phrase_parse;
  using boost::spirit::x3::parse;

  ParseLine(QString(str.data()));
  //if(m.attr.size() > 0)
  //  std::cout << boost::get<AST::Header>(m.attr[0]).level;
  //std::cout << "\nContent : " << m.content << "\n";
}

/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef KMARKDOWNPARSER_GRAMMAR_
#define KMARKDOWNPARSER_GRAMMAR_

#include "annotate_position.hpp"
#include "../ast/blockquote.h"
#include "../ast/header.h"
#include "../ast/string.hpp"
#include "../ast/emphasizedstring.h"
#include "../ast/token.hpp"
#include "../ast/plain_string.hpp"
#include "../ast/attribute.h"
#include "../ast/link.hpp"

#include "boost/variant/variant.hpp"
#include "boost/spirit/home/x3.hpp"

#include <string>

namespace parser {
  namespace x3 = boost::spirit::x3;

  class Bold_Tag;
  class Cancle_Tag;
  class Underline_Tag;
  class Italic_Tag;

  struct BoldTokenType_ : annotate_position {};
  struct CancleTokenType_ : annotate_position {};
  struct UnderlineTokenType_ : annotate_position {};
  struct ItalicTokenType_ : annotate_position {};
  
  using BoldTokenType = 
    x3::rule<BoldTokenType_, AST::Token<AST::EmphasizeType::BOLD>>;
  using CancleTokenType = 
    x3::rule<CancleTokenType_, AST::Token<AST::EmphasizeType::CANCLE>>;
  using UnderlineTokenType = 
    x3::rule<UnderlineTokenType_, AST::Token<AST::EmphasizeType::UNDERLINE>>;
  using ItalicTokenType = 
    x3::rule<ItalicTokenType_, AST::Token<AST::EmphasizeType::ITALIC>>;

  //BlockQuote Start
  struct BlockQuoteTokenType_ : annotate_position {};
  struct BlockQuoteType_ : annotate_position{};

  using BlockQuoteTokenType = 
    x3::rule<BlockQuoteTokenType_, AST::Token<AST::AttrType::BLOCKQUOTE>>;
  using BlockQuoteType = 
    x3::rule<BlockQuoteType_, AST::BlockQuote>;

  //BlockQuote End

  //Link Start
  struct HyperLinkType_ : annotate_position{};
  using HyperLinkType = x3::rule<HyperLinkType_, AST::HyperLink>;

  struct ImgLinkType_ : annotate_position{};
  using ImgLinkType = x3::rule<ImgLinkType_, AST::ImgLink>;

  //Link End

  struct EmphasizeType_ : annotate_position{};
  struct PlainStringType_ : annotate_position{};
  struct EmphasizeString_ : annotate_position{};
  struct Header_ : annotate_position{};
  struct String_ : annotate_position{};

  using EmphasizeType = x3::rule<EmphasizeType_, AST::Emphasize>;
  using PlainStringType = x3::rule<PlainStringType_, AST::PlainString>;
  using ContentType = x3::rule<class Content_, std::string>;
  using EmphasizeStringType = x3::rule<
    class EmphasizeString_, AST::EmphasizeString>;
  using HeaderType = x3::rule<Header_, AST::Header>;
  using StringType = x3::rule<String_, AST::String>;
  
  BOOST_SPIRIT_DECLARE(StringType, HeaderType,  
      EmphasizeType, EmphasizeStringType, PlainStringType,
      BoldTokenType, CancleTokenType, UnderlineTokenType, ItalicTokenType,
      BlockQuoteTokenType, BlockQuoteType, HyperLinkType, ImgLinkType)

};

parser::EmphasizeType       Emphasize();
parser::ContentType         Content();
parser::EmphasizeStringType EmphasizeString();
parser::HeaderType          Header();
parser::StringType          String();

#endif //KMARKDOWNPARSER_GRAMMAR_

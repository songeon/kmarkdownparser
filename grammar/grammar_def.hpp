/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef KMARKDOWNPARSER_GRAMMAR_DEF_
#define KMARKDOWNPARSER_GRAMMAR_DEF_

#include "grammar.hpp"
#include "annotate_position.hpp"

#include "boost/spirit/home/x3.hpp"

#include <iostream>

namespace parser {
  namespace x3 = boost::spirit::x3;
  namespace ascii = boost::spirit::x3::ascii;

  using x3::_attr;
  using x3::double_;
  using x3::int_;
  using x3::lexeme;
  using x3::lit;
  using x3::omit;
  using x3::repeat;
  using x3::_val;
  using ascii::char_;

	template<typename T>
	auto as = [](auto p) { return x3::rule<struct _, T>{} = as_parser(p); };

  BoldTokenType const BoldToken = "BoldToken";
  const auto BoldToken_def = omit[lit("**") | lit("*")];
  BOOST_SPIRIT_DEFINE(BoldToken);

  CancleTokenType const CancleToken = "CancleToken";
  const auto CancleToken_def = omit[lit("~~") | lit("~")];
  BOOST_SPIRIT_DEFINE(CancleToken);

  UnderlineTokenType const UnderlineToken = "UnderlineToken";
  const auto UnderlineToken_def = omit[lit("++") | lit("+")];
  BOOST_SPIRIT_DEFINE(UnderlineToken);

  ItalicTokenType const ItalicToken = "ItalicToken";
  const auto ItalicToken_def = omit[lit("__") | lit("_")];
  BOOST_SPIRIT_DEFINE(ItalicToken);

  //BlockQuote Start
  BlockQuoteTokenType const BlockQuoteToken = "BlockQuoteToken";
  const auto BlockQuoteToken_def = omit[lit("> ")];
  BOOST_SPIRIT_DEFINE(BlockQuoteToken);

  BlockQuoteType const BlockQuote = "BlockQuote";
  const auto BlockQuote_def = BlockQuoteToken;
  BOOST_SPIRIT_DEFINE(BlockQuote);
  //BlockQuote End

  //Emphasize Start
  EmphasizeType const Emphasize = "Emphasize";
  PlainStringType PlainString = "PlainString";

  //PlainString Start

  const auto tokens = char_("*~+_[]()<>!#\\");
  const auto PlainString_def = x3::omit[+(char_ - tokens)];
  BOOST_SPIRIT_DEFINE(PlainString);
  //PlainString End
  
  //Link Start

  HyperLinkType const HyperLink = "HyperLink";
  const auto HyperLink_def =  
    lit("[") >> 
    PlainString >> 
    "](" >> 
    *(char_ - char_(')')) >> 
    ")"; 
  BOOST_SPIRIT_DEFINE(HyperLink);

  ImgLinkType const ImgLink = "ImgLink";
  const auto ImgLink_def =  
    lit("![") >> 
    *(char_ - char_(']')) >> 
    "](" >> 
    *(char_ - char_(')')) >> 
    ")"; 
  BOOST_SPIRIT_DEFINE(ImgLink);

  const auto Link = HyperLink | ImgLink;


  //Link End
  
  const auto Emphasize_def = 
    BoldToken
    | CancleToken
    | UnderlineToken
    | ItalicToken ;

  BOOST_SPIRIT_DEFINE(Emphasize);

  //Emphasize End


  //EmphasizeString Start
  EmphasizeStringType const EmphasizeString = "EmphasizeString";

  const auto setEmpText = [](auto& ctx) {
    _val(ctx).appendText(_attr(ctx));
  };

  const auto setEmp = [](auto& ctx) {
    _val(ctx).addEmphToken(_attr(ctx));
  };

  const auto setHyperLinkText = [](auto& ctx) {
    _attr(ctx).process();
    _val(ctx).appendText(_attr(ctx).text);
    _val(ctx).appendLink(AST::Link(_attr(ctx)));
  };

  const auto EmphasizeString_def = 
    +(PlainString[setEmpText] | HyperLink[setHyperLinkText] | Emphasize[setEmp]);
  BOOST_SPIRIT_DEFINE(EmphasizeString);
  //EmphasizeString End

  //Header Start
  HeaderType const Header = "Header";

  auto HeaderLevel = [](auto& ctx) { 
    _val(ctx).level =  _attr(ctx).size();
  };

  const auto Header_def = 
    repeat(1, 6)[char_("#")][HeaderLevel] >> ' ';
  BOOST_SPIRIT_DEFINE(Header);
  //Header End
  
  //String Start
  StringType const String = "String";

  const auto pushFunc = [](auto& ctx) {
    _val(ctx).attr.push_back(_attr(ctx));
  };

  const auto setContent = [](auto& ctx) {
    std::cout << "Header : " << std::string(_attr(ctx).iterStart, _attr(ctx).iterEnd) << std::endl;
    _val(ctx).appendPlainString(_attr(ctx));
  };

  const auto getEmphStr = [](auto& ctx) { 
    std::vector<AST::PlainString> strs = _attr(ctx).getString();
    std::vector<AST::Emphasize> emphs = _attr(ctx).getEmphasizes();
    _val(ctx).appendPlainStrings(strs);
    _val(ctx).appendEmphasizes(emphs);
    _val(ctx).emphStr = _attr(ctx);
  };

  const auto String_def= 
    EmphasizeString[getEmphStr]
    | ((Header[pushFunc] | *BlockQuote[pushFunc]) >> x3::seek[PlainString[setContent]]);
  BOOST_SPIRIT_DEFINE(String);
  //String End
};

#endif //KMARKDOWNPARSER_GRAMMAR_DEF_

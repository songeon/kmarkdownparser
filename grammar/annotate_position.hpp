#ifndef KMARKDOWNPARSER_GRAMMAR_ANNOTATE_
#define KMARKDOWNPARSER_GRAMMAR_ANNOTATE_
#include "../ast/config.hpp"
#include "../ast/posTagging.hpp"
#include "boost/spirit/home/x3/support/unused.hpp"
#include "boost/spirit/home/x3/support/utility/error_reporting.hpp"
#include "boost/spirit/home/x3.hpp"

#include <iostream>
#include <string>

#include <typeinfo>
#include <type_traits>
namespace parser {
  using namespace boost::spirit;
  struct annotate_position {
    template <typename T, typename Iterator>
      void annotateNonterminal(Iterator const& begin, Iterator const& first, Iterator const& last,T& ast) {
        std::cout << typeid(ast).name() <<": non basic\n";
        using astType =  typename std::decay<decltype(ast)>::type;
        static_assert(std::is_base_of<AST::PosTagging<AST::DefaultIterator>, astType>::value, 
            "all anotated nonterminal must be subclass of AST::PosTagging");
        ast.iterStart = first;
        ast.iterEnd = last;
        ast.posStart = std::distance(begin, first);
        ast.posEnd = std::distance(begin, last) - 1;
        return;
      }

    template <typename T, typename Iterator, typename Context> 
      inline void on_success(Iterator const& first, Iterator const& last, T& ast, Context const& context){
        std::cout << "parsed : '" << std::string(first, last) << "'\n";

        using astType =  typename std::decay<decltype(ast)>::type;
        Iterator begin = x3::get<AST::FirstString_tag>(context).get();

        if constexpr(!std::is_same<astType, x3::unused_type>::value)
          annotateNonterminal(begin, first, last, ast);
      }

    template <typename Iterator, typename Exception, typename Context>
      x3::error_handler_result on_error(
          Iterator& first, Iterator const& last
          , Exception const& x, Context const& context)
      {
        std::cout
          << "Error! Expecting: "
          << x.which()
          << " here: \""
          << std::string(x.where(), last)
          << "\""
          << std::endl
          ;
        return x3::error_handler_result::fail;
      }

  };
};
#endif //KMARKDOWNPARSER_GRAMMAR_ANNOTATE_


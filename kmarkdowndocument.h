#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "textstylefactory.h"

namespace AST {
        class String;
};
class QTextDocument;
class QTextCursor;
class QTextCharFormat;
class QString;

class KMarkdownDocument {
private:
    void renderEmphasize(QTextCursor &, AST::String &);
    void renderAttributeText(QTextCursor &, AST::String &, QTextCharFormat , const QString &);
    void insertText(QTextCursor &, AST::String &, QTextCharFormat, const QString &);
    void insertHtml(QTextCursor &, AST::String &, const QString &, const QString &, const QString &); 
    TextStyleFactory *tsFactory;
    QTextDocument *m_doc = nullptr;

public:
    KMarkdownDocument();
    ~KMarkdownDocument();
    void render(AST::String, const QString &);
    QTextDocument* document();
};

#endif // MAINWINDOW_H

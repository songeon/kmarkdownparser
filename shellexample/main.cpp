/***************************************************************************
 *   Copyright (C) 2002 by Wilco Greven <greven@kde.org>                   *
 *   Copyright (C) 2003 by Christophe Devriese                             *
 *                         <Christophe.Devriese@student.kuleuven.ac.be>    *
 *   Copyright (C) 2003 by Laurent Montel <montel@kde.org>                 *
 *   Copyright (C) 2003-2007 by Albert Astals Cid <aacid@kde.org>          *
 *   Copyright (C) 2004 by Andy Goossens <andygoossens@telenet.be>         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include <QApplication>
#include <QFileDialog>
#include <QMainWindow>

#include <KLocalizedString>
#include <KMessageBox>
#include <KPluginLoader>
#include <KPluginFactory>
#include <kparts/readonlypart.h>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    QMainWindow window;

    KPluginLoader loader(QStringLiteral("kmarkdownparserpart"));
    KPluginFactory *partFactory = loader.factory();

    if (!partFactory)   {
        KMessageBox::error(nullptr, i18n("Unable to find the KMarkdownParser component: %1", loader.errorString()));
    } else {
        KParts::ReadOnlyPart* const firstPart = partFactory->create< KParts::ReadOnlyPart >(&window);
        window.setCentralWidget(firstPart->widget());

        QUrl docUrl = QFileDialog::getOpenFileUrl();

        if (docUrl.isValid() && docUrl.isLocalFile()) {
            firstPart->openUrl(docUrl);
        }
    }

    window.show();

    return app.exec();
}


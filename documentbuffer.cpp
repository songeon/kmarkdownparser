#include "documentbuffer.h"

#include <QTextDocument>
#include <QTextCursor>
#include <QString>
#include <QStringList>
#include <QTextBlock>

#include <iostream>

DocumentBuffer::DocumentBuffer() {
  m_doc = new QTextDocument();
}

DocumentBuffer::~DocumentBuffer() {
    if (m_doc != nullptr) {
        delete m_doc;
        m_doc = nullptr;
    }
}

void DocumentBuffer::setText(const QString &text) {
   QStringList tl = text.split("\n");
   for(const QString &t : tl) {
       std::cout << "splited : " << t.toUtf8().toStdString() << std::endl;
       insertText(t);
   }
}

void DocumentBuffer::insertText(const QString &text) {
    QTextBlock b = m_doc->lastBlock();
    QTextCursor cursor(m_doc);
    cursor.movePosition(QTextCursor::End, QTextCursor::MoveAnchor);
    cursor.beginEditBlock();
    cursor.insertBlock();
    b = b.next();
    cursor.insertText(text);
    cursor.endEditBlock();
    std::cout << "block1 : " << b.text().toUtf8().toStdString() << std::endl;
}

void DocumentBuffer::clear() {
    m_doc->clear();
}

QTextDocument* DocumentBuffer::document() {
    return m_doc;
}

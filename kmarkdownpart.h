#ifndef KMARKDOWN_PART_
#define KMARKDOWN_PART_

#include <KParts/ReadOnlyPart>

#include "kmarkdownparserpart_export.h"

class QWidget;
class QTextEdit;
class QTextDocument;
class DocumentBuffer;
class KMarkdownDocument;

class KMARKDOWNPARSERPART_EXPORT KMarkdownPart : public KParts::ReadOnlyPart {
  Q_OBJECT
  public:
    KMarkdownPart(QWidget *parentWidget, QObject *parent, const QVariantList &args);
    ~KMarkdownPart() override;
    
    QTextEdit* view();
    QTextDocument* document();
    QWidget* widget() override;

    bool openFile() override;

  protected:
    QTextEdit *m_view = nullptr;
    DocumentBuffer *m_buf = nullptr;
    KMarkdownDocument *m_doc = nullptr;
};
#endif //KMARKDOWN_PART_

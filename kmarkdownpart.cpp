#include "kmarkdownpart.h"

#include "ast/string.hpp"
#include "parser.h"
#include "documentbuffer.h"
#include "kmarkdowndocument.h"

#include <QFile>
#include <QWidget>
#include <QTextEdit>
#include <QTextStream>
#include <QtConcurrent>
#include <QFuture>
#include <QString>
#include <QStringList>

#include <KPluginFactory>

#include <vector>


K_PLUGIN_FACTORY(KMarkdownPartFactory, registerPlugin<KMarkdownPart>();)

KMarkdownPart::KMarkdownPart(QWidget 	*parentWidget, QObject	*parent, const QVariantList &args) :
        KParts::ReadOnlyPart (parent) {
    Q_UNUSED(args)
    m_buf = new DocumentBuffer();
    m_view = new QTextEdit(parentWidget);
    m_doc = new KMarkdownDocument();
    m_view->setDocument(m_doc->document());
}

KMarkdownPart::~KMarkdownPart() {
    if (m_buf != nullptr) {
        delete m_buf;
        m_buf = nullptr;
    }
}

bool KMarkdownPart::openFile() {
    QFile *qf = new QFile(localFilePath());
    if (!qf->open(QIODevice::ReadOnly | QIODevice::Text)) {
        return false;
    }
    QTextStream stream(qf);
    QStringList stringList;
    QString buf;

    std::vector<QFuture<AST::String>> parsed;
    
    while(stream.readLineInto(&buf)) {
      stringList.append(buf);
      QFuture<AST::String> fut = QtConcurrent::run(ParseLine, buf);
      parsed.emplace_back(fut);
      fut.waitForFinished();
    }
    m_buf->clear();
    //TODO change to openStream
    qf->close();

    for(int i = 0; i < parsed.size(); ++i) {
      parsed[i].waitForFinished();
      m_doc->render(parsed[i].result(), stringList[i]);
    }
    return true;
}

QTextEdit* KMarkdownPart::view() {
    return m_view;
}

QTextDocument* KMarkdownPart::document() {
    return m_buf->document();
}

QWidget* KMarkdownPart::widget() {
    return view();
}

#include "kmarkdownpart.moc"

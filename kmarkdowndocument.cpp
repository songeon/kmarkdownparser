#include "kmarkdowndocument.h"

#include "ast/string.hpp"
#include "ast/header.h"
#include "textstylefactory.h"
#include "throw_exception.hpp"

#include "boost/variant/get.hpp"

#include <QFileDialog>
#include <QTextDocument>
#include <QTextBlock>
#include <QTextStream>
#include <QTextCharFormat>
#include <QScrollBar>
#include <QStringRef>

#include <iostream>

KMarkdownDocument::KMarkdownDocument() {
    tsFactory = new TextStyleFactory();
    m_doc = new QTextDocument();
}

void KMarkdownDocument::render(AST::String ps, const QString &line) {
  QTextCursor cursor(m_doc);
  cursor.movePosition(QTextCursor::End, QTextCursor::MoveAnchor);
  cursor.insertBlock();

  auto style = tsFactory->getByFlag(TextStyle::Normal);

  renderAttributeText(cursor, ps, style, line);
  std::cout << "parsed len "<< ps.content.size() << " " << ps.emphs.size() << std::endl;

  cursor.movePosition(QTextCursor::StartOfBlock,QTextCursor::MoveAnchor);
  renderEmphasize(cursor, ps);
}

void KMarkdownDocument::renderAttributeText(QTextCursor &cursor,
AST::String &strParsed, QTextCharFormat style, const QString &line) { 
  bool isNoAttr = true;
  QString htmlHead = "";
  QString htmlTail = "";

  for(const auto &attr : strParsed.attr) {
    if(boost::get<AST::Header>(&attr)) {
      isNoAttr = false;
      int level = boost::get<AST::Header>(&attr)->level;
      QString levelStr = QString::number(level, 10);
      htmlHead += "<H" + levelStr + ">";
      htmlTail += "</H" + levelStr + ">";
    } else if(boost::get<AST::BlockQuote>(&attr)) {
        isNoAttr = false;
        htmlHead += "<blockquote> ";
        htmlTail += "</blockquote>";
        std::cout << "blockquote\n";
    }
  }

  if(isNoAttr){
    insertText(cursor, strParsed, style, line);
  }else {
    insertHtml(cursor, strParsed, line, htmlHead, htmlTail);
  }
}

void KMarkdownDocument::insertText(QTextCursor &cursor, AST::String &strParsed, QTextCharFormat style, const QString &line){
  int n = 0;
  for(const auto &pstr : strParsed.content) {
    int len = pstr.posEnd - pstr.posStart + 1;
    QStringRef ref(&line, pstr.posStart, len);
    std::cout << n++ << " : " << ref.toString().toStdString() << std::endl;
    cursor.insertText(ref.toString(),style);
  }
}

void KMarkdownDocument::insertHtml(QTextCursor &cursor, AST::String &strParsed, const QString &line, 
      const QString &htmlHead, const QString &htmlTail){
  int n = 0;
  QString buf = htmlHead;
  for(const auto &pstr : strParsed.content) {
    int len = pstr.posEnd - pstr.posStart + 1;
    QStringRef ref(&line, pstr.posStart, len);
    buf += ref.toString();
  }
  buf += htmlTail;
  cursor.insertHtml(buf);
}


void KMarkdownDocument::renderEmphasize(QTextCursor &cursor,
AST::String &strParsed){
  using namespace AST;
  cursor.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
  int lineStart = cursor.position();

  for(auto &emph : strParsed.emphs) {
    QTextCharFormat modif;
    switch (emph.getEmphType()) {
      case EmphasizeType::BOLD:
        std::cout << "bold\n";
        modif = tsFactory->getByFlag(TextStyle::Bold);
        break;
      case EmphasizeType::CANCLE:
        std::cout << "calcle\n";
        modif = tsFactory->getByFlag(TextStyle::Cancelline);
        break;
      case EmphasizeType::UNDERLINE:
        std::cout << "underline\n";
        modif = tsFactory->getByFlag(TextStyle::Underline);
        break;
      case EmphasizeType::ITALIC:
        std::cout << "italic\n";
        modif = tsFactory->getByFlag(TextStyle::Italic);
        break;
      default:
        std::cout << "not setted\n";
    }
    //QTextCharFormat tokenForm = tsFactory->getByFlag(TextStyle::Token);
    //cursor.setPosition(lineStart + emph.posStart, QTextCursor::MoveAnchor);
    //cursor.setPosition(lineStart + emph.posEnd+1, QTextCursor::KeepAnchor);
    //cursor.setCharFormat(tokenForm);

    if(emph.start != -1) {
      std::cout << "emphs : "<< emph.start << " " << emph.end << std::endl;
      cursor.setPosition(lineStart + emph.start, QTextCursor::MoveAnchor);
      cursor.setPosition(lineStart + emph.end, QTextCursor::KeepAnchor);
      cursor.mergeCharFormat(modif);
    }
    //std::cout << "selected : " << cursor.selectedText().toUtf8().constData() << std::endl;
  }
  cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::MoveAnchor);
}

QTextDocument* KMarkdownDocument::document() {
    return m_doc;
}


KMarkdownDocument::~KMarkdownDocument(){
    tsFactory->deleteLater();
    m_doc->deleteLater();
}

#ifndef KMARKDOWNPARSER_PARSE_
#define KMARKDOWNPARSER_PARSE_

#include "ast/string.hpp"
#include "ast/config.hpp"

class QString;

AST::String ParseLine(const QString& qstr);
//AST::String ParseLine(AST::DefaultIterator begin, AST::DefaultIterator end);

#endif //KMARKDOWNPARSER_PARSE_

#ifndef KMARKDOWNPARSER_AST_PLAIN_STRING_
#define KMARKDOWNPARSER_AST_PLAIN_STRING_
#include "config.hpp"
#include "posTagging.hpp"

namespace AST {
  struct PlainString : PosTagging<DefaultIterator> {};
};

#endif //KMARKDOWNPARSER_AST_PLAIN_STRING_

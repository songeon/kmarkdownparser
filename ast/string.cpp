#include "string.hpp"

namespace AST {
  void String::appendPlainStrings(const std::vector<PlainString> &strs){
    content.reserve(content.size() + strs.size());
    content.insert(content.end(), strs.cbegin(), strs.cend());
  }
  void String::appendEmphasizes(const std::vector<Emphasize> &newEmphs){
    emphs.reserve(emphs.size() + newEmphs.size());
    emphs.insert(emphs.end(), newEmphs.cbegin(), newEmphs.cend());
  }
  void String::appendPlainString(const PlainString &str){
    content.emplace_back(str);
  }

};

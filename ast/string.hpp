#ifndef KMARKDOWNPARSER_AST_STRING_
#define KMARKDOWNPARSER_AST_STRING_

#include "config.hpp"
#include "posTagging.hpp"
#include "plain_string.hpp"
#include "emphasize.h"
#include "emphasizedstring.h"
#include "header.h"
#include "blockquote.h"
#include "link.hpp"

#include "boost/fusion/include/adapt_struct.hpp"
#include "boost/variant/variant.hpp"

#include <vector>

namespace AST{
  using boost::variant;
  using Attribute = variant<Header, BlockQuote>;

  struct String : PosTagging<DefaultIterator>{
    EmphasizeString emphStr;
    std::vector<PlainString> content;
    std::vector<Emphasize> emphs;
    std::vector<Attribute> attr;

    void appendPlainStrings(const std::vector<PlainString> &strs);
    void appendPlainString(const PlainString &str);
    void appendEmphasizes(const std::vector<Emphasize> &newEmphs);
  };
};

BOOST_FUSION_ADAPT_STRUCT(AST::String, emphStr)

#endif //KMARKDOWNPARSER_AST_STRING_

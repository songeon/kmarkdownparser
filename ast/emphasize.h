#ifndef KMARKDOWNPARSER_AST_EMPHASIZE_
#define KMARKDOWNPARSER_AST_EMPHASIZE_

#include "config.hpp"
#include "posTagging.hpp"
#include "token.hpp"
#include "link.hpp"

#include "boost/variant.hpp"
#include "boost/fusion/include/adapt_struct.hpp"

namespace AST {
  using namespace boost::spirit;
  enum class EmphasizeType {
    DEFAULT = 0,
    BOLD,
    CANCLE,
    UNDERLINE,
    ITALIC,
  };

  struct Emphasize : PosTagging<DefaultIterator> {
    EmphasizeType getEmphType() {
      if(boost::get<Token<EmphasizeType::BOLD>>(&tok)) {
        emptType = EmphasizeType::BOLD;
      } else if (boost::get<Token<EmphasizeType::CANCLE>>(&tok)) {
        emptType = EmphasizeType::CANCLE;
      } else if (boost::get<Token<EmphasizeType::UNDERLINE>>(&tok)) {
        emptType = EmphasizeType::UNDERLINE;
      } else if (boost::get<Token<EmphasizeType::ITALIC>>(&tok)) {
        emptType = EmphasizeType::ITALIC;
      }

      return emptType;
    }

    int start = 0, end = 0, bufIdx = 0;
    EmphasizeType emptType = EmphasizeType::DEFAULT;
    boost::variant<
      Token<EmphasizeType::BOLD>, 
      Token<EmphasizeType::CANCLE>,
      Token<EmphasizeType::UNDERLINE>,
      Token<EmphasizeType::ITALIC>
        > tok;
  };
};

BOOST_FUSION_ADAPT_STRUCT(AST::Emphasize, tok)
#endif //KMARKDOWNPARSER_AST_EMPHASIZE_

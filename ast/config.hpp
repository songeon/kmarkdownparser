#ifndef KMARKDOWNPARSER_AST_CONFIG_
#define KMARKDOWNPARSER_AST_CONFIG_
#include <string>
#include <QString>

namespace AST {
//  using DefaultIterator = QString::iterator;
  using DefaultIterator = std::string::iterator;
  class FirstString_tag;//tag for saving begin of string
};

#endif //KMARKDOWNPARSER_AST_CONFIG_

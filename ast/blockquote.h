#ifndef KMARKDOWNPARSER_AST_BLOCKQUOTE_
#define KMARKDOWNPARSER_AST_BLOCKQUOTE_

#include "config.hpp"
#include "posTagging.hpp"
#include "token.hpp"
#include "attribute.h" 
#include "boost/fusion/include/adapt_struct.hpp"

namespace AST{
  struct BlockQuote : PosTagging<DefaultIterator> {
    Token<AttrType::BLOCKQUOTE> tok;
  };
};

BOOST_FUSION_ADAPT_STRUCT(AST::BlockQuote, tok)

#endif //KMARKDOWNPARSER_AST_BLOCKQUOTE_

#ifndef KMARKDOWNPARSER_AST_POSTAGGING_
#define KMARKDOWNPARSER_AST_POSTAGGING_
#include "boost/spirit/home/x3.hpp"
#include "boost/spirit/home/x3/support/ast/position_tagged.hpp"
namespace AST {
  template <typename Iterator>
  struct PosTagging {
    Iterator iterStart;
    Iterator iterEnd;
    int posStart;
    int posEnd;
    PosTagging& operator=(const PosTagging &pos) {
      iterStart = pos.iterStart;
      iterEnd = pos.iterEnd;
      posStart = pos.posStart;
      posEnd = pos.posEnd;
      return *this;
    }
  };
};
#endif //KMARKDOWNPARSER_AST_POSTAGGING_

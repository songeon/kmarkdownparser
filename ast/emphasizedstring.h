/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef KMARKDOWNPARSER_AST_EMPHASIZE_STRING_
#define KMARKDOWNPARSER_AST_EMPHASIZE_STRING_

#include "emphasize.h"
#include "config.hpp"
#include "plain_string.hpp"
#include "link.hpp"

#include "boost/fusion/include/adapt_struct.hpp"

#include <map>
#include <vector>
#include <string>

namespace AST {

  struct Piece {
    int start = 0, len = 0;
    int bufIdx = 0;
  };

  using PieceMap = std::multimap<int, PlainString>;
  using PieceMapIter = PieceMap::iterator;

  class EmphasizeString {
    private:
      int currPos = 0, addedTokenLen = 0;
      bool isDirty = true;
      std::vector<int> countIndex;

      DefaultIterator strStart;
      std::vector<PlainString> buffer;
      std::vector<Emphasize>  empSt, emps;
      std::vector<Link> links;
      PieceMap pieceMap;

    private:
      int clearNonComplete(std::vector<Emphasize>::iterator );
      void processString();
      void processEmphasize();
    public:
      EmphasizeString();
      void initBegin(DefaultIterator begin);
      void addEmphToken(Emphasize &empt);
      //void appendText(const std::string& text);
      void appendText(const PlainString &text);
      std::vector<PlainString> getString();
      const std::vector<Emphasize> &getEmphasizes();
      void appendLink(const Link &lnk);
  };
};

BOOST_FUSION_ADAPT_STRUCT(AST::EmphasizeString)
#endif //KMARKDOWNPARSER_AST_EMPHASIZE_STRING_

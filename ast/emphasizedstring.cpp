/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "emphasizedstring.h"
#include "plain_string.hpp"

#include "boost/range/adaptor/map.hpp"

#include <iostream>

namespace AST{
  EmphasizeString::EmphasizeString() {
  }

  int EmphasizeString::clearNonComplete(std::vector<Emphasize>::iterator nCompIt) {
    int bufTokenLen = 0;
    int dis = std::distance(empSt.begin(), nCompIt);
    for(auto iter = empSt.cbegin()+dis; iter != empSt.cend(); ++iter) {
      PlainString ps;
      ps.posStart = iter->posStart;
      ps.posEnd = iter->posEnd;
      appendText(ps);
    }
    empSt.erase(nCompIt, empSt.end());
    return bufTokenLen;
  }

  void EmphasizeString::addEmphToken(Emphasize &empt){
    isDirty = true;
    for(auto it = empSt.begin(); it != empSt.end(); ++it){
      //if pair found
      if(it->getEmphType() == empt.getEmphType()) {
        clearNonComplete(it+1);

        it->end = pieceMap.size();
        emps.emplace_back(*it);

        empSt.pop_back();
        return;
      }
    }

    empt.start = pieceMap.size();
    empSt.emplace_back(empt);
  }

  void EmphasizeString::appendText(const PlainString &text) {
    isDirty = true;
    pieceMap.insert(std::make_pair(text.posStart,text));
  }

  void EmphasizeString::processString() {
    countIndex.clear();
    buffer.clear();
    clearNonComplete(empSt.begin());

    countIndex.push_back(0);
    auto valueRange = pieceMap | boost::adaptors::map_values;

    std::cout << "countIndex : " << countIndex.back() << " ";
    for(const auto &piece : valueRange) {
      buffer.emplace_back(piece);

      int len = piece.posEnd - piece.posStart + 1;
      std::cout << "(" << piece.posStart << ", " << piece.posEnd << ") ";
      countIndex.push_back(countIndex.back() + len);
      std::cout << countIndex.back() << " ";
    }
    std::cout << std::endl;
    processEmphasize();
    isDirty = false;
  }

  void EmphasizeString::processEmphasize() {
    for(int i = 0; i < emps.size(); ++i) {
      emps[i].start = countIndex[emps[i].start];
      emps[i].end = countIndex[emps[i].end];
    }
  }

  std::vector<PlainString> EmphasizeString::getString(){
    if(isDirty) {
      processString();
    }
    return buffer;
  }

  const std::vector<Emphasize> &EmphasizeString::getEmphasizes() {
    if(isDirty) {
      processString();
    }
    return emps;
  }

  void EmphasizeString::initBegin(DefaultIterator begin) {
    strStart = begin;
  }

  void EmphasizeString::appendLink(const Link &lnk) {
    links.emplace_back(lnk);
  }
};

#ifndef KMARKDOWNPARSER_AST_HEADER_
#define KMARKDOWNPARSER_AST_HEADER_

#include "config.hpp"
#include "posTagging.hpp"
#include "boost/fusion/include/adapt_struct.hpp"

namespace AST{
  struct Header : PosTagging<DefaultIterator>{ 
    long unsigned int level = 0; 
  };
};

BOOST_FUSION_ADAPT_STRUCT(AST::Header, (long unsigned int, level))

#endif //KMARKDOWNPARSER_AST_HEADER_

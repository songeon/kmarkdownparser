#ifndef KMARKDOWNPARSER_AST_TOKEN_
#define KMARKDOWNPARSER_AST_TOKEN_
#include "posTagging.hpp"
#include "config.hpp"
#include "emphasize.h"

namespace AST{
  class EmphasizeTag;
  enum class EmphasizeType;

  template<auto Tag>
  struct Token : PosTagging<DefaultIterator> {
    using TagType = decltype(Tag);
    TagType tag = Tag;
  };
};
#endif //KMARKDOWNPARSER_AST_TOKEN_

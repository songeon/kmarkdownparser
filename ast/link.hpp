#ifndef KMARKDOWNPARSER_AST_LINK_
#define KMARKDOWNPARSER_AST_LINK_

#include "config.hpp"
#include "posTagging.hpp"
#include "plain_string.hpp"
#include "emphasize.h"

#include "boost/fusion/include/adapt_struct.hpp"
#include "boost/variant.hpp"

#include <string>

namespace AST{
  struct HyperLink : PosTagging<DefaultIterator>{ 
   PlainString text;
   std::string src;
   void process() {
     auto linkPos = static_cast<PosTagging<DefaultIterator> *>(this);
     auto textPos = static_cast<PosTagging<DefaultIterator> *>(&text);
     *linkPos = *textPos;
   }
  };

  struct ImgLink : PosTagging<DefaultIterator>{ 
   std::string alt;
   std::string src;
  };

  using Link = boost::variant<HyperLink, ImgLink>;
};

BOOST_FUSION_ADAPT_STRUCT(AST::HyperLink, text, src)
BOOST_FUSION_ADAPT_STRUCT(AST::ImgLink, alt, src)

#endif //KMARKDOWNPARSER_AST_LINK_

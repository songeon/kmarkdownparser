#ifndef KMARKDOWNPARSER_DOCUMENTBUFFER_
#define KMARKDOWNPARSER_DOCUMENTBUFFER_

class QTextDocument;
class QString;

class DocumentBuffer{
  public:
    DocumentBuffer();
    ~DocumentBuffer();
    void setText(const QString &text);
    void insertText(const QString &text);
    void clear();
    QTextDocument* document();

  private:
    QTextDocument *m_doc = nullptr;
};
#endif //KMARKDOWNPARSER_DOCUMENTBUFFER_


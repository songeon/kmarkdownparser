/*
* Copyright 2019  Son Geon kde.jen6@gmail.com
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "parser.h"

#include "throw_exception.hpp"
#include "grammar/grammar.hpp"
#include "grammar/grammar_def.hpp"

#include "boost/spirit/home/support/char_encoding/standard.hpp"
#include "boost/spirit/home/x3/support/utility/error_reporting.hpp"
#include "boost/spirit/home/x3.hpp"

#include <QString>

#include <string>
#include <exception>
#include <iostream>

AST::String ParseLine(const QString& qstr) {
  using boost::spirit::x3::parse;
  using boost::spirit::x3::with;
  using boost::spirit::x3::error_handler_tag;
  using error_handler_type = boost::spirit::x3::error_handler<std::string::iterator>;

  std::string str = qstr.toUtf8().constData();
  auto begin = str.begin();
  auto iter= str.begin();
  auto end = str.end();

  error_handler_type error_handler(iter, end, std::cerr);


  AST::String m = AST::String{};
  auto withparser = with<AST::FirstString_tag>(std::ref(begin))
  [
    with<error_handler_tag>(std::ref(error_handler)) 
    [
    parser::String
    ]
  ];

  try{
    bool r = parse(begin, str.end(), withparser, m);
    if(begin != str.end()) {
      std::cout << "unparsed part : " << std::string(begin, end) << std::endl;
    }
  }catch(std::exception const& e) {
    std::cerr << e.what() << std::endl;
  }

  return m;
}

//AST::String ParseLine(AST::DefaultIterator begin, AST::DefaultIterator end) {
//  using boost::spirit::x3::parse;
//  using boost::spirit::x3::with;
//  AST::String m{};
//  auto withparser = with<AST::FirstString_tag>(std::ref(begin))
//    [
//      parser::String
//    ];
//  parse(begin, end, withparser, m);
//  return m;
//}

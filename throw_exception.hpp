#ifndef KMARKDOWNPARSER_THROW_EXCEPTION_
#define KMARKDOWNPARSER_THROW_EXCEPTION_

#include "boost/exception/exception.hpp"

#ifdef BOOST_NO_EXCEPTIONS
#include <cstdlib> // std::abort
#include <exception>
#include <iostream>

namespace boost {
// dummy implementation for user-defined function from boost/throw_exception.hpp
inline void throw_exception(std::exception const& e) {
  std::cerr << e.what() << std::endl;
  std::abort();
}
} 

#endif //BOOST_NO_EXCEPTIONS
#endif //KMARKDOWNPARSER_THROW_EXCEPTION_
